/**
 * Created by limei on 2018/5/10.
 */

export default {
  map: {
    domain: 'http://minedata.cn',
    spriteUrl: this.domain + '/minemapapi/v1.3/sprite/sprite',
    fontUrl: this.domain + '/minemapapi/v1.3/fonts',
    solu: 4209,
    // solu: 5473,
    token: 'eeea35fb6c984047bb19e25463ee18e5',
    // token: '6310d22d85744399808d3bd49be42e84',
    get style() {
      return `${this.domain}/service/solu/style/id/${this.solu}`;
    },
    // center: [116.46,39.92],
    center: [103.47131555530729,38.5268590621408],
    zoom: 3.3,
  },
  service: {
    root_local: 'http://172.21.1.135:8005/indexService',
    // root: 'http://113.106.54.36:10005/indexService',
    // root: '//172.21.0.228:8005/indexService',
    root: '//36.111.82.89:8005/indexService',
    // 全国指数排名-00/01  --实时
    get realIndex() {
      return this.root + '/ranking/city/realtime/'
    },
    // 全国指数排名-00/01   -历史
    get historyIndex() {
      return this.root + '/ranking/city/history'
    },
    //目标城市详情 - 02
    get cityInfo() {
      return this.root + '/basic/city/realtime/'
    },
    //目标城市详情 --历史-- - 02
    get cityInfo_history() {
      return this.root + '/basic/city/history/'
    },
    //行政区指数详情  - 03
    get cityArea() {
      return this.root + '/ranking/cityArea/realtime/'
    },
    //行政区指数详情  ---历史--- 03
    get cityArea_history() {
      return this.root + '/ranking/cityArea/history/'
    },
    //行政区道路指数概况 - 04
    get roadBasic() {
      return this.root + '/basic/road/realtime/'
    },
    //行政区道路指数概况 --历史--- 04
    get roadBasic_history() {
      return this.root + '/basic/road/history/'
    },
    //行政区道路指数排名 - 04
    get roadList() {
      return this.root + '/ranking/road/realtime/'
    },
    //行政区道路指数排名   --历史 -- - 04
    get roadList_history() {
      return this.root + '/ranking/segroad/history/'
    },
 /* ************** 地图相关请求 ********************* */
    //全国视图
    get globalIndex() {
      return this.root + '/countryViewRT/data/'
    },
    //行政区面
    get polygon() {
      return this.root + '/mapview/tile/realtime/03/{z}/{x}/{y}/'
    },
    //行政区面---历史
    get polygon_history() {
      return this.root + '/mapview/tile/history/03/{z}/{x}/{y}'
    },
    //网格
    get mapGrid() {
      return this.root + '/mapview/tile/realtime/04/{z}/{x}/{y}/'
    },
    //网格 --- 历史
    get mapGrid_history() {
      return this.root + '/mapview/tile/history/04/{z}/{x}/{y}'
    },
    //道路级别
    get roadLine() {
      return this.root + '/mapview/tile/realtime/05/{z}/{x}/{y}/'
    },
    //道路级别 ---历史
    get roadLine_history() {
      return this.root + '/mapview/tile/history/05/{z}/{x}/{y}'
    },
    //偶发拥堵
    get occCongest() {
      return this.root + '/mapview/tile/ocjam/{z}/{x}/{y}/'
    },
    //常发拥堵
    get oftenCongest() {
      return this.root + '/mapview/tile/aljam/{z}/{x}/{y}'
    },
    //seg路段-实时
    get segLine() {
      return this.root + '/mapview/segroad'
    },
    //实时路况测试
    get realRoadTest() {
      return this.root + '/checkRoadIndexRT'
    },
  },


  // 其他相关常量
  // 用于全国视图fitBounds
  bboxList:[[75.62890479743427,19.81996084379888],[134.44433230798427,52.95656602588471]],
  //初始常量
  CUR_CITY:{
    cityname: "北京市",
    code: "110000",
    index: 1.8,
    ranking: 1,
    speed: "0",
    standard: "0",
    centerX: 116.385807268724,
    centerY: 39.9243551058029
  },
  CUR_DISTRICT:{
    name:'全市',
    code:'110000'
  },
  CUR_ROAD_STATE:'real',  //初始实时路况
  CUR_INDEX_TYPE:'01',   //初始延时指数
  HISTORY_TYPE:'month',   //初始历史按月分析
  HISTORY_TIME:{
    year:2018,
    month:new Date().getMonth()+1 < 10 ? '0'+(new Date().getMonth()+1):new Date().getMonth()+1,
  },   //初始延时指数
  DATE_TYPE:'work',   //初始是否工作日
  MAP_LEVEL:'01',   //初始地图是全国视图
  HISTORY_HOUR:new Date().getHours(),   //当前小时
  CUR_TIME_STAMP:null,

  //各指数范围
  // DELAY_RANGE:[1.4,1.5,1.7],
  // JAM_RANGE:[4.0,6.0,8.0],
  // SPACE_RANGE:[6.0,7.0,8.0],
  DELAY_RANGE:[1.3,1.6,1.9,2.2],
  JAM_RANGE:[4.0,8.0,11.0,14.0],
  SPACE_RANGE:[6.0,12.0,18.0,24.0],

  ROAD_RANGE:[1.3,1.6,1.9,2.2],
  //各指数范围对应颜色
  INDEX_COLORS:['#3baa14','#64e60d','#ffb200','#ff6312','#ff003b'],
  INDEX_COLORS2:['rgba(48, 159, 9, 1)','rgba(123, 230, 0, 1)','rgba(255,220,80,1)', 'rgba(249,125,41,1)', 'rgba(255,0,59,1)'],
  // INDEX_COLORS2:['#3baa14','#64e60d','#ffb200','#ff6312','#ff003b'],

  // 各视图等级对应-地图zoom级别
  MAX_MAP_LEVEL:18,
  GLOBAL_MAP_LEVEL:3.3,
  DIS_MAP_LEVEL:10,
  GRI_MAP_LEVEL:11,
  CONGEST_MAP_LEVEL:10,
  ROAD_MAP_LEVEL:12,
  AUTO_MAP_LEVEL:10,
  SEG_MAP_LEVEL:14,
}
