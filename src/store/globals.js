/**
 * Created by limei on 2018/5/10.
 */
import MYCONF from '../myconf'
export default {
  state: {
    cur_city: MYCONF.CUR_CITY,     //当前城市
    cur_district:MYCONF.CUR_DISTRICT,  //当前行政区划
    cur_state: MYCONF.CUR_ROAD_STATE,  //当前路况Tab(实时/历史)
    cur_index_type: MYCONF.CUR_INDEX_TYPE,  //当前指数类型
    history_type: MYCONF.HISTORY_TYPE,  //历史路况年月选择
    history_time: MYCONF.HISTORY_TIME,  //历史路况-设置时间(年/月)
    date_type: MYCONF.DATE_TYPE,  //工作日/非工作日/全部
    map_level: MYCONF.MAP_LEVEL,  //地图级别：全国视图/行政区划/网格/道路
    history_hour: MYCONF.HISTORY_HOUR,  //历史路况下:小时设置
    cur_timestamp: MYCONF.CUR_TIME_STAMP,  //当前请求下的统一时间戳
    server_timestamp: MYCONF.CUR_TIME_STAMP,  //当前请求下的统一时间戳
  },

  getters: {
    cur_city: state => state.cur_city,
    cur_city_name: state => state.cur_city.cityname,
    cur_city_code: state => state.cur_city.code,
    cur_district: state => state.cur_district,
    cur_state: state => state.cur_state,
    cur_index_type: state => state.cur_index_type,
    history_type: state => state.history_type,
    history_time: state => state.history_time,
    date_type: state => state.date_type,
    map_level: state => state.map_level,
    history_hour: state => state.history_hour,
    cur_timestamp: state => state.cur_timestamp,
    server_timestamp: state => state.server_timestamp,
  },

  mutations: {
    updateCurCity(state, payload) {
      state.cur_city = Object.assign({},state.cur_city,payload);
    },
    updateCurDistrict(state, payload) {
      state.cur_district = payload;
    },
    updateRoadType(state, payload) {
      state.cur_state = payload;
    },
    updateIndexType(state, payload) {
      state.cur_index_type = payload;
    },
    updateHistoryType(state, payload) {
      state.history_type = payload;
    },
    updateHistoryTime(state, payload) {
      state.history_time = Object.assign({},state.history_time,payload);
    },
    updateDateType(state, payload) {
      state.date_type = payload;
    },
    updateMapLevel(state, payload) {
      state.map_level = payload;
    },
    updateHistoryHour(state, payload) {
      state.history_hour = payload;
    },
    updateCurTimestamp(state, payload) {
      state.cur_timestamp = payload;
    },
    updateServerTimestamp(state, payload) {
      state.server_timestamp = payload;
    },
  },

}

