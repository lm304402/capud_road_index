/**
 * Created by limei on 2018/5/10.
 */
import MYCONF from '../../myconf'
export default {
  state: {
    index_max: 10,     //当前城市
    district_index_max:10,  //当前行政区
    cur_road_kind:0,    //当前道路类型
    cur_segId:'',    //当前seg路段id
  },

  getters: {
    index_max: state => state.index_max,
    district_index_max: state => state.district_index_max,
    cur_road_kind: state => state.cur_road_kind,
    cur_segId: state => state.cur_segId,
  },

  mutations: {
    updateIndexMax(state, payload) {
      state.index_max = payload;
    },
    updateDistrictIndexMax(state, payload) {
      state.district_index_max = payload;
    },
    updateCurRoadKind(state, payload) {
      state.cur_road_kind = payload;
    },
    updateCurSegId(state, payload) {
      state.cur_segId = payload;
    }
  },

}

