/**
 * Created by limei on 2018/5/10.
 */
export default {
  state: {
    isPanelShow: true,
    isProvinceshow:false,
    isCityshow:false,

    firstCityCode:'510100',
    secondCityCode:'210200'
  },

  getters: {
    isPanelShow: state => state.isPanelShow,
    isProvinceshow: state => state.isProvinceshow,
    isCityshow: state => state.isCityshow,

    firstCityCode: state => state.firstCityCode,
    secondCityCode: state => state.secondCityCode,
  },

  mutations: {
    updatePanelState(state, payload) {
      state.isPanelShow = payload;
    },
    updateProvinceState(state, payload) {
      state.isProvinceshow = payload;
    },
    updateCityState(state, payload) {
      state.isCityshow = payload;
    },
    upfirstCityCode(state, payload){
      state.firstCityCode = payload;
    },
    upsecondCityCode(state, payload){
      state.secondCityCode = payload;
    }
  },

}
