/**
 * Created by limei on 2018/5/11.
 */
import Vue from 'vue'
import Vuex from 'vuex'
import globals from './globals'
import diagram from './modules/diagram'
import roadPanel from './modules/road-panel'


Vue.use(Vuex)

export default new Vuex.Store({
  modules:{
    globals,
    diagram,
    roadPanel,
  }
})
