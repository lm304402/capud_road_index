/**
 * Created by limei on 2018/5/14.
 */
import MYCONF from '../myconf'
class MarkerHelper {

  static addBlingMarker(map, coordinate, param, indexType) {    //01格式
    let indexRange;
    let markerColor;
    switch (indexType) {
      case '02':
        indexRange = MYCONF.JAM_RANGE;
        break;
      case '03':
        indexRange = MYCONF.SPACE_RANGE;
        break;
      default:
        indexRange = MYCONF.DELAY_RANGE;
        break;
    }
    if (param < indexRange[0]) {
      markerColor = 'base'
    } else if (param >= indexRange[0] && param < indexRange[1]) {
      markerColor = 'green'
    } else if (param >= indexRange[1] && param < indexRange[2]) {
      markerColor = 'yellow'
    } else if (param >= indexRange[2] && param < indexRange[3]) {
      markerColor = 'line'
    } else {
      markerColor = 'red'
    }

    let el = document.createElement('div');
    el.id = 'marker';
    el.className = `minemap-marker bling bling-${markerColor}`;
    el.style.cursor = "pointer";

    return el;
  }

}

export default MarkerHelper;


