const pingNianDays = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
const runNianDays = [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

class DateTimeHelper {

  static formTime(time) {    //01格式
    if (time < 10) {
      return "0" + time;
    } else {
      return time;
    }
  }
  static formMinutes(minute) {    //01格式
    let tenNum = parseInt(minute/10);
    let singleNum = minute%10;
    if (singleNum >= 5) {
      return tenNum+"5";
    } else {
      return tenNum+'0';
    }
  }

  static formCurTime() {         //5分钟
    let date = new Date();
    return date.getFullYear() + "" + this.formTime(date.getMonth() + 1) + this.formTime(date.getDate()) +  this.formTime(date.getHours()) + this.formTime(date.getMinutes()) + this.formTime(date.getSeconds());
  }

  static getPreTime(time){
    let hour = parseInt(time.slice(8,10));
    let minute = parseInt(time.slice(10,12));
    let curMinute = this.formMinutes(minute);
    if(curMinute === '00'){
      return this.formTime(parseInt(hour) - 1) + ":55";
    }else{
      return this.formTime(parseInt(hour)) + ":" + this.formTime(parseInt(curMinute) - 5);
    }
  }

  static getCurTime(time){
    let hour = parseInt(time.slice(8,10));
    let minute = parseInt(time.slice(10,12));
    return hour+ ':'+this.formMinutes(minute);
  }

  static getPreHour(time){
    let hour = parseInt(time.slice(8,10));
    return (parseInt(hour)-1) + ':00';
  }

  static getCurHour(time){
    let hour = parseInt(time.slice(8,10));
    return hour+ ':00';
  }
}

export default DateTimeHelper;
