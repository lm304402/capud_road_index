import MYCONF from '../myconf'
class colorHelper {

  static formTime(time) {    //01格式
    if (time < 10) {
      return "0" + time;
    } else {
      return time;
    }
  }
  static formMinutes(minute) {    //01格式
    let tenNum = parseInt(minute/10);
    let singleNum = minute%5;
    if (singleNum >= 5) {
      return tenNum+"5";
    } else {
      return tenNum+'0';
    }
  }

static setIndexColor(item,cur_index_type){
  let indexRange = MYCONF.DELAY_RANGE;
  switch (cur_index_type){
    case '01':
      indexRange = MYCONF.DELAY_RANGE;
      break;
    case '02':
      indexRange = MYCONF.JAM_RANGE;
      break;
    case '03':
      indexRange = MYCONF.SPACE_RANGE;
      break;
    default:
      indexRange = MYCONF.DELAY_RANGE;
      break;
  }
  if(parseFloat(item) < indexRange[0]){
    return 'baseIndex';
  }else if(parseFloat(item) >= indexRange[0] && parseFloat(item) < indexRange[1]){
    return 'greenIndex';
  }else if(parseFloat(item) >= indexRange[1] && parseFloat(item) < indexRange[2]){
    return 'yellowIndex';
  }else if(parseFloat(item) >= indexRange[2] && parseFloat(item) < indexRange[3]){
    return 'lineIndex';
  }else if(item === 1000){
    return 'baseIndex';
  }else if(item >= indexRange[3]){
    return 'redIndex';
  }else{
    return 'baseIndex';
  }
}

}

export default colorHelper;
