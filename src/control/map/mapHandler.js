/**
 * Created by limei on 2018/5/10.
 */
import MYCONF from '../../myconf'
import markerHelper from '../../util/marker-helper'
import eventBus from '../../util/event-bus'

export default class mapHandler {
  constructor(_view) {
    this._view = _view;
    this.map = _view.map;
  }

  loadImages() {
    const map = this.map;
    let mapIMgs = ['local.gif', 'logo.png'];
    mapIMgs.map((item) => {
      let imgUrl = './static/images/mapImg/' + item;
      map.loadImage(imgUrl, function (error, image) {
        if (error) throw error;
        map.addImage(item.split(".")[0], image);
      })
    });


    this._view.$http.get('//minedata.cn/service/solu/' + 8410 + '?token=b141ca81b7f14b08ab59a4622bf2ec90')
      .then(response => {
        response = response.body;
        console.log(response);
      }, response => {
        // error callback
      });
  }

  setMapEvent(){
    let map = this.map;
    let _this = this._view;
    // map.on('click', function (e) {
    //   if (!map.getLayer('citySymbol')) return;
    //   let feas = map.queryRenderedFeatures(e.point, {
    //     layers: ['citySymbol']
    //   });
    //   console.log('feas', feas);
    //   if (!feas.length) return;
    //   _this._view.updateMapLevel('02');
    //   const coordinate = feas[0].geometry.coordinates;
    // })

    map.on('zoomend', function () {
      let curZoom =map.getZoom();
      _this.curZoom = map.getZoom();
      // console.log("当前级别——",map.getZoom(),_this.map_level);
      if(_this.map_level === '01'){
        map.setMaxZoom(5);
      }else{
        map.setMaxZoom(17);
      }
      if(_this.map_level === '04' || _this.map_level === '06'){
        let maxKind = curZoom < 11 ?2:curZoom < 12 ?3:curZoom < 14 ?4:5;
        if(map.getLayer('roadLine')) map.setFilter('roadLine',['<=', 'class', maxKind]);
      }

      //自动切换图例
      if(_this.map_level === '06' && curZoom >= MYCONF.GRI_MAP_LEVEL){
        eventBus.$emit('roadLegendShow',true);
        eventBus.$emit('roadPoygonShow',false);
      }else if(_this.map_level === '06' && curZoom <= MYCONF.AUTO_MAP_LEVEL){
        eventBus.$emit('roadPoygonShow',true);
        eventBus.$emit('roadLegendShow',false);
      }else{
        eventBus.$emit('roadLegendShow',false);
        eventBus.$emit('roadPoygonShow',false);
      }

      map.on('mousemove',function (e) {
        map.getCanvas().style.cursor = 'pointer';
        if (_this.map_level === '04' || _this.map_level === '06') {
          if (!map.getLayer('roadLine')) return;
          let features = map.queryRenderedFeatures(e.point, {
            layers: ['roadLine'],
          });
          if (features && features.length > 0) {
            map.getCanvas().style.cursor = 'pointer';
          } else {
            map.getCanvas().style.cursor = '';
          }
        }
      });
      map.on('click',function (e) {
        console.log(e.lngLat.toArray())
        if (_this.map_level === '04') {
          if (!map.getLayer('roadLine')) return;
          let feas = map.queryRenderedFeatures(e.point, {
            layers: ['roadLine'],
          });
          if (!feas.length) return;
          console.log(feas);
        }
      });
    })
  }

  highlightPoygon(name,code){
    const map = this.map;
    if(name === '全市'){
      if(map.getLayer('polygon-highlight')) map.setFilter('polygon-highlight', ['==', 'code', '']);
    }else{
      if(map.getLayer('polygon-highlight')) map.setFilter('polygon-highlight', ['==', 'code', code]);
    }
  }
  filterRoadLine(kind){
    const map = this.map;
    // if(map.getLayer('polygon-highlight')) map.setFilter('polygon-highlight', ['==', 'code', '']);
  }

}
