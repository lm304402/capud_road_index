/**
 * Created by limei on 2018/5/10.
 */
import MYCONF from '../../myconf'
import markerHelper from '../../util/marker-helper'

export default class layerController {
  constructor(_view) {
    this._view = _view;
    this.map = _view.map;
  }

  getPolygonSource() {
    let url = MYCONF.service.polygon;
    let timeStamp = this._view.cur_timestamp;
    let indexType = this._view.cur_index_type;
    let code = this._view.cur_city.code;
    this._view.$http.get(url + `02/${indexType}/${timeStamp}?cityCode=${code}`)
      .then(response => {
        response = response.body;
        this._view.polyonSource = response.result;
        return response.result;
      }, response => {
        // error callback
      });
  }

//行政区-面
   addPolynLayer() {
    let map = this.map;
    let timeStamp = this._view.cur_timestamp;
    let indexType = this._view.cur_index_type;

     let code = this._view.cur_city.code;
     let weekType = this._view.date_type === 'all'?'A':this._view.date_type === 'work'?'W':'H';
     let hour = this._view.history_hour<10?'0'+this._view.history_hour:this._view.history_hour;
     let year = this._view.history_time.year;
     let month = this._view.history_time.month;
     // let month = parseInt(this._view.history_time.month)>10?this._view.history_time.month:'0'+this._view.history_time.month;
     let analyTime = this._view.history_type === 'year'?`year=${year}`:`year=${year}&month=${month}`;
     let url =this._view.cur_state==='real'?MYCONF.service.polygon+ timeStamp + '?indexType=' + indexType + '&cityCode=' + this._view.cur_city.code
       :MYCONF.service.polygon_history+`?indexType=${indexType}&cityCode=${code}&holidayTag=${weekType}&hour=${hour}&${analyTime}`;

     let mapLevel = this._view.map_level;
    let minZoom = MYCONF.GLOBAL_MAP_LEVEL;
    let maxZoom = mapLevel==='06'?MYCONF.AUTO_MAP_LEVEL:MYCONF.MAX_MAP_LEVEL;
     let delay_markerMap = [[0, MYCONF.INDEX_COLORS2[0]], [MYCONF.DELAY_RANGE[0], MYCONF.INDEX_COLORS2[1]], [MYCONF.DELAY_RANGE[1], MYCONF.INDEX_COLORS2[2]], [MYCONF.DELAY_RANGE[2], MYCONF.INDEX_COLORS2[3]], [MYCONF.DELAY_RANGE[3], MYCONF.INDEX_COLORS2[4]]];
     let jam_markerMap = [[0, MYCONF.INDEX_COLORS2[0]], [MYCONF.JAM_RANGE[0], MYCONF.INDEX_COLORS2[1]], [MYCONF.JAM_RANGE[1], MYCONF.INDEX_COLORS2[2]], [MYCONF.JAM_RANGE[2], MYCONF.INDEX_COLORS2[3]], [MYCONF.JAM_RANGE[3], MYCONF.INDEX_COLORS2[4]]];
     let space_markerMap = [[0, MYCONF.INDEX_COLORS2[0]], [MYCONF.SPACE_RANGE[0], MYCONF.INDEX_COLORS2[1]], [MYCONF.SPACE_RANGE[1], MYCONF.INDEX_COLORS2[2]], [MYCONF.SPACE_RANGE[2], MYCONF.INDEX_COLORS2[3]], [MYCONF.SPACE_RANGE[3], MYCONF.INDEX_COLORS2[4]]];
     let polyonMap = this._view.cur_index_type === '01' ? delay_markerMap : this._view.cur_index_type === '02' ? jam_markerMap : space_markerMap;
    // if (this._view.polyonSource) {
    // console.log(this._view.polyonSource.length);
    if (map.getSource('polygon')) map.removeSource('polygon');
    // map.addSource("polygon", {
    //   "type": "geojson",
    //   "data": {
    //     "type": "FeatureCollection",
    //     "features": this._view.polyonSource
    //   }
    // });
    map.addSource("polygon", {
      type: "vector",
      tiles: [url]
    });

    if (map.getLayer('polygon')) map.removeLayer('polygon');
    map.addLayer({
      'id': 'polygon',
      'type': 'fill',
      'source': 'polygon',
      'source-layer': 'indexLayer',
      'layout': {},

      'paint': {
        'fill-color': {
          "type": "interval",
          "property": "index",
          "stops": polyonMap
        },
        'fill-opacity': 0.5,
        'fill-outline-color': 'rgba(255, 255, 255, 0.4)'
      },
      "minzoom": minZoom,
      "maxzoom": maxZoom,
    });
     map.addLayer({
       'id': 'polygon-highlight',
       'type': 'fill',
       'source': 'polygon',
       'source-layer': 'indexLayer',
       'layout': {},

       'paint': {
         'fill-color': {
           "type": "interval",
           "property": "index",
           "stops": polyonMap
         },
         'fill-opacity': 0.7,
         'fill-outline-color': 'rgba(255, 255, 255, 0.4)'
       },
       "minzoom": minZoom,
       "maxzoom": maxZoom,
       "filter": ["==", "code", ""]
     });
    // if (map.getLayer('polygonSymbol')) map.removeLayer('polygonSymbol');
    // map.addLayer({
    //   "id": "polygonSymbol",
    //   "type": "symbol",
    //   "source": 'polygon',
    //   'source-layer': 'indexLayer',
    //   "layout": {
    //     "text-field": "{name}",
    //     "text-offset": [0, -0.6],
    //     "text-anchor": "top",
    //     "icon-allow-overlap": false,  //图标允许压盖
    //     "text-allow-overlap": true,   //图标覆盖文字允许压盖
    //   },
    //   "paint": {
    //     "text-color": '#ddd'
    //   }
    // });
    // if (map.getLayer('indexSymbol')) map.removeLayer('indexSymbol');
    /*map.addLayer({
     "id": "indexSymbol",
     "type": "symbol",
     "source": 'polygon',
     'source-layer': 'indexLayer',
     "layout": {
     "text-field": "{index}",
     "text-offset": [0, 0.6],
     "text-anchor": "top",
     "icon-allow-overlap": true,  //图标允许压盖
     "text-allow-overlap": true,   //图标覆盖文字允许压盖
     },
     "paint": {
     "text-color": '#363636'
     }
     });*/
    // }
  }

//行政区网格
  addGridLayer() {
    let map = this.map;
    let timeStamp = this._view.cur_timestamp;
    let indexType = this._view.cur_index_type;

    let code = this._view.cur_city.code;
    let weekType = this._view.date_type === 'all'?'A':this._view.date_type === 'work'?'W':'H';
    let hour = this._view.history_hour<10?'0'+this._view.history_hour:this._view.history_hour;
    let year = this._view.history_time.year;
    let month = this._view.history_time.month;
    // let month = parseInt(this._view.history_time.month)>10?this._view.history_time.month:'0'+this._view.history_time.month;
    let analyTime = this._view.history_type === 'year'?`year=${year}`:`year=${year}&month=${month}`;
    let url =this._view.cur_state==='real'?MYCONF.service.mapGrid+ timeStamp + '?indexType=' + indexType + '&cityCode=' + this._view.cur_city.code
      :MYCONF.service.mapGrid_history+`?indexType=${indexType}&cityCode=${code}&holidayTag=${weekType}&hour=${hour}&${analyTime}`;

    let mapLevel = this._view.map_level;
    let minZoom = mapLevel==='06'?MYCONF.AUTO_MAP_LEVEL:MYCONF.GLOBAL_MAP_LEVEL;
    let maxZoom = mapLevel==='06'?MYCONF.GRI_MAP_LEVEL:MYCONF.MAX_MAP_LEVEL;
    let delay_markerMap = [[0, MYCONF.INDEX_COLORS2[0]], [MYCONF.DELAY_RANGE[0], MYCONF.INDEX_COLORS2[1]], [MYCONF.DELAY_RANGE[1], MYCONF.INDEX_COLORS2[2]], [MYCONF.DELAY_RANGE[2], MYCONF.INDEX_COLORS2[3]], [MYCONF.DELAY_RANGE[3], MYCONF.INDEX_COLORS2[4]]];
    let jam_markerMap = [[0, MYCONF.INDEX_COLORS2[0]], [MYCONF.JAM_RANGE[0], MYCONF.INDEX_COLORS2[1]], [MYCONF.JAM_RANGE[1], MYCONF.INDEX_COLORS2[2]], [MYCONF.JAM_RANGE[2], MYCONF.INDEX_COLORS2[3]], [MYCONF.JAM_RANGE[3], MYCONF.INDEX_COLORS2[4]]];
    let space_markerMap = [[0, MYCONF.INDEX_COLORS2[0]], [MYCONF.SPACE_RANGE[0], MYCONF.INDEX_COLORS2[1]], [MYCONF.SPACE_RANGE[1], MYCONF.INDEX_COLORS2[2]], [MYCONF.SPACE_RANGE[2], MYCONF.INDEX_COLORS2[3]], [MYCONF.SPACE_RANGE[3], MYCONF.INDEX_COLORS2[4]]];
    let polyonMap = this._view.cur_index_type === '01' ? delay_markerMap : this._view.cur_index_type === '02' ? jam_markerMap : space_markerMap;
    if (this._view.polyonSource) {
      // console.log(this._view.polyonSource.length);
      if (map.getSource('gridLine')) map.removeSource('gridLine');
      map.addSource("gridLine", {
        type: "vector",
        tiles: [url]
      });

      if (map.getLayer('gridLine')) map.removeLayer('gridLine');
      map.addLayer({
        'id': 'gridLine',
        'type': 'fill',
        'source': 'gridLine',
        'source-layer': 'indexLayer',
        'layout': {},
        'paint': {
          'fill-color': {
            "type": "interval",
            "property": "grid",
            "stops": polyonMap
          },
          'fill-opacity': 0.6,
          'fill-outline-color': 'rgba(255, 255, 255, 0.4)'
        },
        "minzoom": minZoom,
        "maxzoom": maxZoom,
      });
      /*if (map.getLayer('gridSymbol')) map.removeLayer('gridSymbol');
       map.addLayer({
       "id": "gridSymbol",
       "type": "symbol",
       "source": 'gridLine',
       'source-layer': 'indexLayer',
       "layout": {
       "text-field": "{name}",
       "text-offset": [0, 0.6],
       "text-anchor": "top",
       "icon-allow-overlap": true,  //图标允许压盖
       "text-allow-overlap": true,   //图标覆盖文字允许压盖
       },
       "paint": {
       "text-color": '#eee'
       }
       });*/
      if (map.getLayer('gridSymbol')) map.removeLayer('gridSymbol');
      map.addLayer({
        "id": "gridSymbol",
        "type": "symbol",
        "source": 'gridLine',
        'source-layer': 'indexLayer',
        "layout": {
          "text-field": "{index}",
          // "text-offset": [-0.6, 0.6],
          "text-anchor": "center",
          // "text-halo-width": 1.0,
          "text-allow-overlap": false,   //图标覆盖文字允许压盖
        },
        "paint": {
          "text-color": '#eee'
        },
        "minzoom": minZoom,
        "maxzoom": maxZoom,
      });
    }
  }

  // getSymbolSource() {
  //   let url = MYCONF.service.globalIndex;
  //   // let timeStamp = this._view.cur_timestamp;
  //   // todo:目前只造的0505的数据
  //   let timeStamp = 201805110505;
  //   // this._view.$http.get(url + this._view.cur_index_type+'/'+timeStamp)
  //   this._view.$http.get('./static/data/global.json')
  //     .then(response => {
  //       response = response.body;
  //       console.log("+++++++++++++++++");
  //       this._view.symbolSource = response.result;
  //       this.addSymbolMarkers(response.result);
  //       return response.result;
  //     }, response => {
  //       // error callback
  //     });
  // }

  // 全国视图闪烁markers
  addSymbolMarkers(symbolSource) {
    symbolSource.map((symbol) => {
      let coordinate = symbol.geometry.coordinates;
      let param = symbol.properties.index;
      let el = markerHelper.addBlingMarker(this.map, coordinate, param, this._view.cur_index_type);
      let marker = new minemap.Marker(el, {offset: [-15, -15]})
        .setLngLat(coordinate)
      this._view.globalMarkers.push(marker);
      el.addEventListener('click', () => {
        let curCode = symbol.properties.code;
        this._view.indexList.map((item) => {
          if (item.code === curCode) {
            this._view.updateCurCity(item);
          }
        });
        if(curCode === this._view.cur_city.code){
          this._view.updateMapLevel('06');
        }
      });
    });
    // console.log(this._view.globalMarkers.length);
  }

  // 全国视图name图层
  addSymbols() {
    let map = this.map;
    let delay_markerMap = [[0, MYCONF.INDEX_COLORS2[0]], [MYCONF.DELAY_RANGE[0], MYCONF.INDEX_COLORS2[1]], [MYCONF.DELAY_RANGE[1], MYCONF.INDEX_COLORS2[2]], [MYCONF.DELAY_RANGE[2], MYCONF.INDEX_COLORS2[3]], [MYCONF.DELAY_RANGE[3], MYCONF.INDEX_COLORS2[4]]];
    let jam_markerMap = [[0, MYCONF.INDEX_COLORS2[0]], [MYCONF.JAM_RANGE[0], MYCONF.INDEX_COLORS2[1]], [MYCONF.JAM_RANGE[1], MYCONF.INDEX_COLORS2[2]], [MYCONF.JAM_RANGE[2], MYCONF.INDEX_COLORS2[3]], [MYCONF.JAM_RANGE[3], MYCONF.INDEX_COLORS2[4]]];
    let space_markerMap = [[0, MYCONF.INDEX_COLORS2[0]], [MYCONF.SPACE_RANGE[0], MYCONF.INDEX_COLORS2[1]], [MYCONF.SPACE_RANGE[1], MYCONF.INDEX_COLORS2[2]], [MYCONF.SPACE_RANGE[2], MYCONF.INDEX_COLORS2[3]], [MYCONF.SPACE_RANGE[3], MYCONF.INDEX_COLORS2[4]]];
    let markerMap = this._view.cur_index_type === '01' ? delay_markerMap : this._view.cur_index_type === '02' ? jam_markerMap : space_markerMap;
    // if (map.getSource('citySymbol')) map.removeSource('citySymbol');
    // map.addSource("citySymbol", {
    //   "type": "geojson",
    //   "data": {
    //     "type": "FeatureCollection",
    //     "features": this._view.symbolSource
    //   }
    // });
    // if (map.getLayer('citySymbol')) map.removeLayer('citySymbol');
    // map.addLayer({
    //   "id": "citySymbol",
    //   "type": "symbol",
    //   "source": 'citySymbol',
    //   "layout": {
    //     "text-field": "{name}",
    //     "text-offset": [0, 0.6],
    //     "text-anchor": "top",
    //     "icon-allow-overlap": true,  //图标允许压盖
    //     "text-allow-overlap": true,   //图标覆盖文字允许压盖
    //   },
    //   "paint": {
    //     "text-color": {
    //       // "type": "exponential",
    //       "property": "index",
    //       "stops": markerMap
    //     }
    //   }
    // });
    // map.addLayer({
    //   "id": "cityIndex",
    //   "type": "symbol",
    //   "source": 'citySymbol',
    //   "layout": {
    //     "text-field": "{index}",
    //     "text-offset": [0, 1.6],
    //     "text-anchor": "top",
    //     "icon-allow-overlap": true,  //图标允许压盖
    //     "text-allow-overlap": true,   //图标覆盖文字允许压盖
    //   },
    //   "paint": {
    //     "text-color": '#fff'
    //   }
    // });

    this._view.globalMarkers.map((marker) => {
      marker.addTo(map);
    })
  }

  //添加道路
  addRoads() {
    let map = this.map;
    let mapLevel = this._view.map_level;
    let delay_lineMap = [[0, '#3a7b4c'], [MYCONF.ROAD_RANGE[0], '#7be600'], [MYCONF.ROAD_RANGE[1], '#e8e548'], [MYCONF.ROAD_RANGE[2], '#f97d29'], [MYCONF.ROAD_RANGE[2], '#ff003b']];
    let timeStamp = this._view.cur_timestamp;
    let indexType = this._view.cur_index_type;

    let code = this._view.cur_city.code;
    let weekType = this._view.date_type === 'all'?'A':this._view.date_type === 'work'?'W':'H';
    let hour = this._view.history_hour<10?'0'+this._view.history_hour:this._view.history_hour;
    let year = this._view.history_time.year;
    let month = this._view.history_time.month;
    // let month = parseInt(this._view.history_time.month)>10?this._view.history_time.month:'0'+this._view.history_time.month;
    let analyTime = this._view.history_type === 'year'?`year=${year}`:`year=${year}&month=${month}`;
    let url =this._view.cur_state==='real'?MYCONF.service.roadLine+ timeStamp + '?indexType=' + indexType + '&cityCode=' + this._view.cur_city.code
      :MYCONF.service.roadLine_history+`?indexType=${indexType}&cityCode=${code}&holidayTag=${weekType}&hour=${hour}&${analyTime}`;

    let minZoom = mapLevel==='06'?MYCONF.GRI_MAP_LEVEL:MYCONF.GLOBAL_MAP_LEVEL;
    let maxZoom = MYCONF.MAX_MAP_LEVEL;
    if (map.getSource('roadLine')) map.removeSource('roadLine');
    map.addSource("roadLine", {
      type: "vector",
      tiles: [url]
    });

    if (map.getLayer('roadLine')) map.removeLayer('roadLine');
    map.addLayer({
      "id": "roadLine",
      "type": "line",
      "source": "roadLine",
      'source-layer': 'indexLayer',
      "layout": {
        "line-join": "round",
        "line-cap": "round"
      },
      // "paint": {
      //   "line-color": "#ff0000",
      //   "line-width": 3
      // },
      "paint": {
        "line-color": {
          "type": "interval",
          "property": "index",
          "stops": delay_lineMap
        },
        "line-width": 2
      },
      "minzoom": minZoom,
      "maxzoom": maxZoom,
      "filter": ["<=", "class", 2]
    },'84c0d2ccd8324ee59a91cd1e6a236e66');//84c0d2ccd8324ee59a91cd1e6a236e66

    //TODO:路况切换逻辑备用-如果实时路况出不来，切换成历史
    let hisUrl = MYCONF.service.realRoadTest;
    let _this = this;
    this._view.$http.get(hisUrl+`?timeStamp=${this._view.cur_timestamp}`).then((res) => {
      res = res.body;
      let bool = res.success;
      if(bool){  //请求到数据
        return ;
      }else{
        _this.addHistoryRoads();
      }
    });
  }

  //添加历史道路-此为实时路况失败时切换逻辑
  addHistoryRoads() {
    let map = this.map;
    let mapLevel = this._view.map_level;
    let delay_lineMap = [[0, '#3a7b4c'], [MYCONF.ROAD_RANGE[0], '#7be600'], [MYCONF.ROAD_RANGE[1], '#e8e548'], [MYCONF.ROAD_RANGE[2], '#f97d29'], [MYCONF.ROAD_RANGE[2], '#ff003b']];
    let timeStamp = this._view.cur_timestamp;
    let indexType = this._view.cur_index_type;

    let code = this._view.cur_city.code;
    let weekType = this._view.date_type === 'all'?'A':this._view.date_type === 'work'?'W':'H';
    let hour = this._view.history_hour<10?'0'+this._view.history_hour:this._view.history_hour;
    let year = this._view.history_time.year;
    let month = this._view.history_time.month;
    // let month = parseInt(this._view.history_time.month)>10?this._view.history_time.month:'0'+this._view.history_time.month;
    let analyTime = this._view.history_type === 'year'?`year=${year}`:`year=${year}&month=${month}`;
    let url =MYCONF.service.roadLine_history+`?indexType=${indexType}&cityCode=${code}&holidayTag=${weekType}&hour=${hour}&${analyTime}`;

    let minZoom = mapLevel==='06'?MYCONF.GRI_MAP_LEVEL:MYCONF.GLOBAL_MAP_LEVEL;
    let maxZoom = MYCONF.MAX_MAP_LEVEL;
    if (map.getSource('roadLine')) map.removeSource('roadLine');
    map.addSource("roadLine", {
      type: "vector",
      tiles: [url]
    });

    if (map.getLayer('roadLine')) map.removeLayer('roadLine');
    map.addLayer({
      "id": "roadLine",
      "type": "line",
      "source": "roadLine",
      'source-layer': 'indexLayer',
      "layout": {
        "line-join": "round",
        "line-cap": "round"
      },
      // "paint": {
      //   "line-color": "#ff0000",
      //   "line-width": 3
      // },
      "paint": {
        "line-color": {
          "type": "interval",
          "property": "index",
          "stops": delay_lineMap
        },
        "line-width": 2
      },
      "minzoom": minZoom,
      "maxzoom": maxZoom,
      "filter": ["<=", "class", 2]
    },'84c0d2ccd8324ee59a91cd1e6a236e66');//84c0d2ccd8324ee59a91cd1e6a236e66
  }

  addCongestion() {
    let map = this.map;
    let timeStamp = this._view.cur_timestamp;
    let weekType = this._view.date_type === 'all'?'A':this._view.date_type === 'work'?'W':'H';
    let hour = this._view.history_hour<10?'0'+this._view.history_hour:this._view.history_hour;
    let year = this._view.history_time.year;
    let month = this._view.history_time.month;
    // let month = parseInt(this._view.history_time.month)>10?this._view.history_time.month:'0'+this._view.history_time.month;
    let analyTime = this._view.history_type === 'year'?`year=${year}`:`year=${year}&month=${month}`;
    // let timeStamp = 201805221815;
    let code = this._view.cur_city.code;
    let url = this._view.cur_state==='real'?MYCONF.service.occCongest+ timeStamp + '?cityCode=' + code:
      MYCONF.service.oftenCongest+`?cityCode=${code}&holidayTag=${weekType}&hour=${hour}&${analyTime}`;   //?cityCode=110000
    if (map.getSource('ocCongest')) map.removeSource('ocCongest');
    map.addSource("ocCongest", {
      type: "vector",
      tiles: [url]
    });

    if (map.getLayer('ocCongest')) map.removeLayer('ocCongest');
    map.addLayer({
      "id": "ocCongest",
      "type": "line",
      "source": "ocCongest",
      'source-layer': 'indexLayer',
      "layout": {
        "line-join": "round",
        "line-cap": "round"
      },
      "paint": {
        "line-color": "#ff003b",
        "line-width": 3
      }
    },'84c0d2ccd8324ee59a91cd1e6a236e66');

  }

  addSegLine(){
    let url = MYCONF.service.segLine;
    // let url = 'http://172.21.13.58:8005/indexService/mapview/segroad?segid=kai3xuan2da4jie1bai2yang2xi1lu4fu3lu4yan2zhou1lu4';
    let cur_segId = this._view.cur_segId;
    let timeStamp = this._view.cur_timestamp;
    let indexType = this._view.cur_index_type;
    let code = this._view.cur_city.code;
    this._view.$http.get(url+"?segid="+cur_segId)
      .then(response => {
        response = response.body;
        this._view.segSource = response.result.geometry;
        if(response.result.centerX) this._view.seg_center = [response.result.centerX,response.result.centerY];
        this.addSegLayer();
        return response.result;
      }, response => {
        // error callback
      });
  }
  addSegLayer(){
    let map = this.map;
    let delay_lineMap = [[0, '#3a7b4c'], [MYCONF.ROAD_RANGE[0], '#7be600'], [MYCONF.ROAD_RANGE[1], '#e8e548'], [MYCONF.ROAD_RANGE[2], '#f97d29'], [MYCONF.ROAD_RANGE[2], '#ff003b']];
    if (map.getSource('segLine')) map.removeSource('segLine');
    map.addSource("segLine", {
      "type": "geojson",
      "data": {
        "type": "Feature",
        "properties": {},
        "geometry": this._view.segSource
      }
    });
    if (map.getLayer('segLine')) map.removeLayer('segLine');
    map.addLayer({
      "id": "segLine",
      "type": "line",
      "source": "segLine",
      "layout": {
        "line-join": "round",
        "line-cap": "round"
      },
      "paint": {
        "line-color": "#d016ff",
        "line-width": 4
      }
    });

    if(this._view.segSource && this._view.segSource.coordinates[0]){
      // map.flyTo({
      //   center:this._view.seg_center.length > 0?this._view.seg_center:this._view.segSource.coordinates[0][0],
      //   zoom:13,
      //   speed:1.6
      // });
      let coorList = [];
      let pointList = [];
      this._view.segSource.coordinates.map((item) => {
        coorList.push(...item);
      });
      coorList.map((item) => {
        pointList.push([item[0],item[1]]);
      });
      let roadLine = turf.lineString(pointList);
      let bbox = turf.bbox(roadLine);

      map.stop();//停止正在进行的动画渐变
      map.fitBounds([[bbox[0], bbox[1]], [bbox[2], bbox[3]]], {
        linear: false,
        padding: {
          top: 200,
          bottom: 200,
          left: 300,
          right: 300
        },
        speed:1.6,
        maxZoom:15
      });
    }
  }

  //删除所有已添加图层
  cleanMapLayer() {
    // alert("在此进行清除~~~~");
    const layerList = [
      'citySymbol',
      'polygon',
      'polygon-highlight',
      'polygonSymbol',
      'indexSymbol',
      'roadLine',
      'gridLine',
      'gridSymbol',
      'ocCongest',
      'segLine'
    ];
    const map = this.map;
    layerList.map((item) => {
      if (map.getSource(item)) {
        map.removeSource(item)
      }
      if (map.getLayer(item)) {
        map.removeLayer(item)
      }
    });

    this._view.polygonSource = [];
    //清除marker(全国视图下)
    map.removeMarkers(this._view.globalMarkers);
    this._view.globalMarkers = [];
  }
}
