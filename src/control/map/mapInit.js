/**
 * Created by limei on 2018/5/10.
 */
import MYCONF from '../../myconf'
import layerController from './layerController'
import mapHandler from './mapHandler'

export default function mapInit() {
  minemap.accessToken = MYCONF.map.token;
  minemap.solution = MYCONF.map.solu;
  minemap.domainUrl = MYCONF.map.domain;
  this.map = new minemap.Map({
    container: 'map-container',
    style: MYCONF.map.style,
    center: MYCONF.map.center,
    zoom: MYCONF.map.zoom,
    pitch: 0,
    // maxZoom: 17, //地图最大缩放级别限制
    maxZoom: 17, //地图全国视图-限制最大缩放级别
    minZoom: 3
  });
  this.layer = new layerController(this);
  this.handler = new mapHandler(this);

  this.map.on('load', () => {
    this.handler.loadImages();
    this.handler.setMapEvent();
    // this.layer.getSymbolSource();
  })
//   this.map.on('click',(e) => {
//     console.log(e)
//   })

}
