
/**
 * Created by liwenxuan on 2018/6/6.
 */
class DataController {
   dataCompletion(standard,data,newdata){
     let hour='';
     if(data.length==0) return newdata;
     for(let i=0;i<data.length;i++){
       if(data[i].timestamp){
          hour= data[i].timestamp.slice(8);
       }else{
          hour = data[i].hour;
       }

       if(hour==standard[newdata.length]){
         newdata.push(parseFloat(data[i].index).toFixed(2));
       }else{
         i--;
         let index = standard.indexOf(hour);
         let num = index - newdata.length;
         for(let j=0;j<num;j++){
           if(data[index-1]){
             newdata.push(parseFloat(data[index-1].index).toFixed(2));
           }else{
             newdata.push(parseFloat(data[0].index).toFixed(2));
           }
         }
       }
     }
     return newdata;
   }
   dataCompletLeft(standard,data,newdata){
     let hour='';
     if(data.length==0) return newdata;
     for(let i=0;i<data.length;i++){
       if(data[i].timestamp){
         let h = data[i].timestamp.slice(8,10);
         let m = data[i].timestamp.slice(10);
         hour = h+":"+m;
       }else{
         let h = data[i].min.slice(0,2);
         let m = data[i].min.slice(2);
         hour = h+":"+m;
       }

       if(hour==standard[newdata.length]){
         newdata.push(parseFloat(data[i].index).toFixed(2));
       }else{
         i--;
         let index = standard.indexOf(hour);
         let num = index - newdata.length;
         for(let j=0;j<num;j++){
           if(data[index-1]){
             newdata.push(parseFloat(data[index-1].index).toFixed(2));
           }else{
             newdata.push(parseFloat(data[0].index).toFixed(2));
           }
         }
       }
     }

     let timestamp = Date.parse(new Date());
     let date = new Date();
     date.setTime(timestamp);
     let mm = date.getMinutes();
     mm = mm < 10 ? ('0' + mm) : mm;

     let length = data.length;
     if(data[length-1].timestamp){
       if( mm - data[length-1].timestamp.slice(10)>8){
         newdata.push(parseFloat(data[length-1].index).toFixed(2));
       }
     }else{
       if( mm - data[length-1].min.slice(2)>8){
         newdata.push(parseFloat(data[length-1].index).toFixed(2));
       }
     }
     return newdata;
   }
}
export default DataController
