/**
 * Created by limei on 2018/5/12.
 */
import MYCONF from '../myconf';
import eventBus from '../util/event-bus'

class RoadController{
  constructor(_view){
    this._view = _view;
  }

  getCityInfo(){
    let timeStamp = this._view.cur_timestamp;
    let code = this._view.cur_city.code;
    let weekType = this._view.date_type === 'all'?'A':this._view.date_type === 'work'?'W':'H';
    let hour = this._view.history_hour<10?'0'+this._view.history_hour:this._view.history_hour;
    let indexType = this._view.cur_index_type;
    let year = this._view.history_time.year;
    let month = this._view.history_time.month;
    // let month = parseInt(this._view.history_time.month)>10?this._view.history_time.month:'0'+this._view.history_time.month;
    let analyTime = this._view.history_type === 'year'?`year=${year}`:`year=${year}&month=${month}`;
    let url =this._view.cur_state==='real'?MYCONF.service.cityInfo+ code+'/'+ this._view.cur_index_type+'/'+timeStamp
      :MYCONF.service.cityInfo_history+ code+'/'+ this._view.cur_index_type+`?holidayTag=${weekType}&hour=${hour}&${analyTime}`;

    this._view.$http.get(url)
      .then(response => {
        response = response.body;
        if(response.success){
          let result = response.result;
          this._view.chartData = response.result;
          this._view.updateCurCity({index:result.index,ranking:result.ranking,speed:result.ranking});
        }else{
        }
      }, response => {
        // error callback
      });
  }

  getDistrictData(){
    let timeStamp = this._view.cur_timestamp;
    let code = this._view.cur_city.code;
    let weekType = this._view.date_type === 'all'?'A':this._view.date_type === 'work'?'W':'H';
    let hour = this._view.history_hour<10?'0'+this._view.history_hour:this._view.history_hour;
    let indexType = this._view.cur_index_type;
    let year = this._view.history_time.year;
    let month = this._view.history_time.month;
    // let month = parseInt(this._view.history_time.month)>10?this._view.history_time.month:'0'+this._view.history_time.month;
    let analyTime = this._view.history_type === 'year'?`year=${year}`:`year=${year}&month=${month}`;
    let url =this._view.cur_state==='real'?MYCONF.service.cityArea+ code+'/'+ this._view.cur_index_type+'/'+timeStamp
      :MYCONF.service.cityArea_history+ code+'/'+ this._view.cur_index_type+`?holidayTag=${weekType}&hour=${hour}&${analyTime}`;

    let indexList = [];
    this._view.$http.get(url)
      .then(response => {
        response = response.body;
        if(response.success){
          this._view.districtData = response.result
          this._view.districtData.map((item) => {
            indexList.push(item.index);
          });
          this._view.updateDistrictIndexMax(Math.max(...indexList));
        }else{
          // console.log(response);
        }
      }, response => {
        // error callback
      });
  }
  getRoadInfoData(){
    let timeStamp = this._view.cur_timestamp;
    let code = this._view.cur_district.code;
    let weekType = this._view.date_type === 'all'?'A':this._view.date_type === 'work'?'W':'H';
    let hour = this._view.history_hour<10?'0'+this._view.history_hour:this._view.history_hour;
    let indexType = this._view.cur_index_type;
    let year = this._view.history_time.year;
    let month = this._view.history_time.month;
    // let month = parseInt(this._view.history_time.month)>10?this._view.history_time.month:'0'+this._view.history_time.month;
    let analyTime = this._view.history_type === 'year'?`year=${year}`:`year=${year}&month=${month}`;
    let url =this._view.cur_state==='real'?MYCONF.service.roadBasic+ code+'/'+ this._view.cur_index_type+'/'+timeStamp
      :MYCONF.service.roadBasic_history+ code+'/'+ this._view.cur_index_type+`?holidayTag=${weekType}&hour=${hour}&${analyTime}`;

    this._view.$http.get(url)
      .then(response => {
        response = response.body;
        if(response.success){
          this._view.roadInfoData = response.result
        }else{
          // console.log(response);
        }
      }, response => {
        // error callback
      });
  }

  getRoadList(){
    // let url =MYCONF.service.roadList;
    // let url ='http://172.21.2.75:8005/indexService/ranking/road/realtime/';

    let timeStamp = this._view.cur_timestamp;
    let code = this._view.cur_district.code;
    let weekType = this._view.date_type === 'all'?'A':this._view.date_type === 'work'?'W':'H';
    let hour = this._view.history_hour<10?'0'+this._view.history_hour:this._view.history_hour;
    let indexType = this._view.cur_index_type;
    let year = this._view.history_time.year;
    let month = this._view.history_time.month;
    // let month = parseInt(this._view.history_time.month)>10?this._view.history_time.month:'0'+this._view.history_time.month;
    let analyTime = this._view.history_type === 'year'?`year=${year}`:`year=${year}&month=${month}`;
    let url =this._view.cur_state==='real'?MYCONF.service.roadList+ code+'/'+ this._view.cur_index_type+'/'+timeStamp
      :MYCONF.service.roadList_history+ code+`?holidayTag=${weekType}&hour=${hour}&${analyTime}`;

    this._view.$http.get(url)
    // this._view.$http.get('http://172.21.2.75:8005/indexService/ranking/road/realtime/420100/01/20180517164100')
      .then(response => {
        response = response.body;
        if(response.success){
          this._view.roadData = response.result
        }else{
          // console.log(response);
        }
      }, response => {
        // error callback
      });
  }

  getSegLineRendered(){

  }
}
export default RoadController
