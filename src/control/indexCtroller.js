/**
 * Created by limei on 2018/5/12.
 */
import MYCONF from '../myconf';
import eventBus from '../util/event-bus'

class IndexController {
  constructor(_view) {
    this._view = _view;
  }

  getIndexList() {
    let timeStamp = this._view.cur_timestamp;
    let weekType = this._view.date_type === 'all' ? 'A' : this._view.date_type === 'work' ? 'W' : 'H';
    let hour = this._view.history_hour < 10 ? '0' + this._view.history_hour : this._view.history_hour;
    let indexType = this._view.cur_index_type;
    let year = this._view.history_time.year;
    let month = this._view.history_time.month;
    // let month = parseInt(this._view.history_time.month) > 10 ? this._view.history_time.month : '0' + this._view.history_time.month;
    let analyTime = this._view.history_type === 'year' ? `year=${year}` : `year=${year}&month=${month}`;
    let url = this._view.cur_state === 'real' ? MYCONF.service.realIndex + this._view.cur_index_type + '/' + timeStamp
      : MYCONF.service.historyIndex + `?holidayTag=${weekType}&hour=${hour}&indexType=${indexType}&${analyTime}`;

    // this._view.indexList = [];
    this._view.$http.get(url)
    // this._view.$http.get('http://172.21.2.40:8005/indexService/ranking/city/realtime/01/201805152300')
      .then(response => {
        response = response.body;
        if (response.success && response.result.length > 0){
          this._view.indexList = response.result;
          this._view.updateServerTimestamp(response.timestamp);
        }

        if (response.result.length === 0 || !response.success) {
          eventBus.$emit('updateIndexList');
        }
        // console.log(response)
      }, response => {
        // error callback
      });
  }

  setIndexColor(item) {
    let index = item.index;
    let cur_index_type = this._view.cur_index_type;
    let indexRange = MYCONF.DELAY_RANGE;
    switch (cur_index_type) {
      case '01':
        indexRange = MYCONF.DELAY_RANGE;
        break;
      case '02':
        indexRange = MYCONF.JAM_RANGE;
        break;
      case '03':
        indexRange = MYCONF.SPACE_RANGE;
        break;
      default:
        indexRange = MYCONF.DELAY_RANGE;
        break;
    }
    if(parseFloat(index) < indexRange[0]){
      return 'baseIndex';
    }else if(parseFloat(index) >= indexRange[0] && parseFloat(index) < indexRange[1]){
      return 'greenIndex';
    }else if(parseFloat(index) >= indexRange[1] && parseFloat(index) < indexRange[2]){
      return 'yellowIndex';
    }else if(parseFloat(index) >= indexRange[2] && parseFloat(index) < indexRange[3]){
      return 'lineIndex';
    }else{
      return 'redIndex';
    }
  }

}
export default IndexController
